$(document).ready(function () {
    dynamicFormBuilder(page1Data,"#page-container-1");
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);
        if ($target.parent().hasClass('disabled')) {
            return false;
        }		
    });
	
	$(".submit-step").click(function (e) {
		var formData = $("#employee_form").serializeObject();
		$('#messageModal').modal();
		console.log(formData);
		$("#formData").empty();
		$("#formData").append(JSON.stringify(formData));
	});
    $(".next-step").click(function (e) {
        var $active = $('.wizard .nav-tabs li.active');
		var stepUrl = $active.children()[0].href;
		var step = stepUrl.substring(stepUrl.indexOf('#'), stepUrl.length);
		$(step+' :visible:input').validate();
		if(!$(step+' :visible:input').valid()){
			return;
		}else{
			var pageData = {};
			var pageContainer = '';
			if(step=="#page1"){
				pageData=page2Data;
				pageContainer = "#page-container-2";
			}else if(step=="#page2"){
				pageData=page3Data;
				pageContainer = "#page-container-3";
			}
			dynamicFormBuilder(pageData,pageContainer);
		}
        $active.next().removeClass('disabled').addClass('active');
		$active.removeClass('active').addClass('disabled');
		$active.next().find('a[data-toggle="tab"]').click();        
    });
    $(".prev-step").click(function (e) {
       var $active = $('.wizard .nav-tabs li.active');
	   $active.prev().removeClass('disabled').addClass('active');
	   $active.removeClass('active').addClass('disabled');		  
       $active.prev().find('a[data-toggle="tab"]').click();
    });
	
	$.validator.addMethod("regularexpression",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return re.test(value.trim());
        },
        "Please provide valid input."
	);
});


function dynamicFormBuilder(jsonName,pageID){
	if(!$(pageID).find('div').length>0){
		$(pageID).empty();
		var generatedHTML = generateFormFromJSON(jsonName);
		$(pageID).append(generatedHTML);
	}
}

function generateFormFromJSON(jsonName){
	var result='';
	if(jsonName!=undefined && jsonName!=null && jsonName!=''){
		var jsonData = JSON.parse(jsonName);
		if(jsonData.columns !=undefined && jsonData.columns!=null){
			var columnlength = jsonData.columns.length;
			if(columnlength>0){
				for(i=0;i<columnlength;i++){
					var required = false;
					if(jsonData.columns[i].required!=undefined){
						required = jsonData.columns[i].required;
					}
					if(jsonData.columns[i].type=="text" || jsonData.columns[i].type=="number" || jsonData.columns[i].type=="email"){
						var minlength = '',maxlength = '',regularexpression ='',min='',max='';
						if(jsonData.columns[i].maxvalue!=undefined){
							max = "data-rule-max='" + jsonData.columns[i].maxvalue+ "'";
						}
						if(jsonData.columns[i].minvalue!=undefined){
							min = "data-rule-min='" + jsonData.columns[i].minvalue+ "'";
						}
						if(jsonData.columns[i].minlength!=undefined){
							minlength = "data-rule-minlength='" + jsonData.columns[i].minlength+ "'";
						}							
						if(jsonData.columns[i].maxlength!=undefined){
							maxlength = "maxlength='" + jsonData.columns[i].maxlength + "'";
						}
						
						if(jsonData.columns[i].regularexpression!=undefined){
							regularexpression = "data-rule-regularexpression='" + jsonData.columns[i].regularexpression + "'";
						}	
						result +=	"<div class='form-group'>"+
									"<label for='"+jsonData.columns[i].id +"'>"+jsonData.columns[i].label +"</label>"+
									"<input type='"+ jsonData.columns[i].type+ "' class='form-control' name='"+jsonData.columns[i].id +"' id='"+jsonData.columns[i].id +"' placeholder='Enter "+jsonData.columns[i].label +"' required='"+ required +"'"+ min + max + minlength + maxlength + regularexpression +"></div>";
					}else if(jsonData.columns[i].type=="textarea"){
							var minlength = '',maxlength = '',rows='';
							if(jsonData.columns[i].minlength!=undefined){
								minlength = "data-rule-minlength='" + jsonData.columns[i].minlength+ "'";
							}							
							if(jsonData.columns[i].maxlength!=undefined){
								maxlength = "data-rule-maxlength='" + jsonData.columns[i].maxlength + "'";
							}
							if(jsonData.columns[i].rows!=undefined){
								rows = "rows='" + jsonData.columns[i].rows + "'";
							}
							
							result +=	"<div class='form-group'>"+
										"<label for='"+jsonData.columns[i].id +"'>"+jsonData.columns[i].label +"</label>"+
										"<textarea type='text' class='form-control' name='"+jsonData.columns[i].id +"' id='"+jsonData.columns[i].id +"' placeholder='Enter "+jsonData.columns[i].label +"' required='"+ required +"'"+  rows + minlength + maxlength +"></textarea></div>";
					}
					else if(jsonData.columns[i].type=="selectbox"){
						    var options = jsonData.columns[i].options;
							var option_value = "<option value=''>Select</option>";
							if(options!=undefined){
								if(options.length>=0){
									for(j=0;j<options.length;j++){
										option_value += "<option value='"+ options[j].value+"'>"+ options[j].value+"</option>";
									}
								}
							}
							result +=  "<div class='form-group'>" + 
									 "<label for='"+jsonData.columns[i].id +"'>"+jsonData.columns[i].label +"</label>"+
									  "<select class='form-control' name='"+jsonData.columns[i].id +"' id='"+jsonData.columns[i].id +"' required='"+ required +"'>"+option_value + "</select></div>"
					}else if(jsonData.columns[i].type=="radio" || jsonData.columns[i].type=="checkbox"){
						var options = jsonData.columns[i].options;
							var option_value = "";	
							var name=	jsonData.columns[i].id						
							if(jsonData.columns[i].type=="checkbox" ){
								name = jsonData.columns[i].id + "[]";
							}
							if(options!=undefined){
								if(options.length>=0){
									for(j=0;j<options.length;j++){
										option_value += "<div class='form-check'>"+
										   			 "<input class='form-check-input' type='"+ jsonData.columns[i].type +"' name='" +name +"' id='" + jsonData.columns[i].id + j + "' value='"+ options[j].value +"'>"+
														 "<label class='form-check-label' for='"+jsonData.columns[i].id +"'>"+ options[j].value +"</label></div>";
									}
								}
							}
						result += "<div class='form-group'>" + 
									 "<label for='"+jsonData.columns[i].id +"'>"+jsonData.columns[i].label +"</label>"+ option_value + "</div>";						
					}
				}
			}
		}
	}
	return result;
}
var page1Data = `{
    "columns": [{
			"label": "Full Name",
			"id": "full_name",
			"type": "text",
			"required": true,
			"minlength" : 2,
			"maxlength" : 100,
			"regularexpression" : "^[a-zA-Z-][a-zA-Z -]*$"
		},
		{
			"label": "Gender",
			"id": "gender",
			"type": "radio",
			"options" : [{"value":"Male"},{"value":"Female"}]
		},
		{
			"label": "Age",
			"id" :"age",
			"type": "text",
			"required": true,
			"maxlength" : 2,
			"minvalue" : 18,
			"maxvalue" : 99
		},
		{
			"label": "Educational Qualification",
			"id": "educational_qualification",
			"type": "selectbox",
			"options" : [{"value":"Post Graduate"},{"value":"Under Graduate"},{"value":"Others"}]
		},
		
		{
			"label": "Languages Known",
			"id": "languages",
			"type": "checkbox",
			"options" : [{"value":"English"},{"value":"Tamil"},{"value":"Telugu"},{"value":"Hindi"}]
		}
		]
}`;

var page2Data = `{
    "columns": [{
			"label": "Address Line 1",
			"id": "address_line1",
			"type": "textarea",
			"required": true,
			"minlength" : 1,
			"maxlength" : 500,
			"rows" : 3			
		},
		{
			"label": "Address Line 2",
			"id": "address_line2",
			"type": "textarea",
			"required": true,
			"minlength" : 1,
			"maxlength" : 500,
			"rows" : 3
		},
		
		{
			"label": "City",
			"id" :"city",
			"type": "text",
			"required": true
		},
		{
			"label": "State",
			"id" :"state",
			"type": "text",
			"required": true
		},
		{
			"label": "Pincode",
			"id" :"pincode",
			"type": "text",			
			"maxlength" : 6
		}
		]
}`;

var page3Data = `{
    "columns": [
		{
			"label": "Mobile Number",
			"id" :"mobile_number",
			"type": "text",
			"required": true,
			"maxlength" : 10,
			"minlength" : 10			
		},
		{
			"label": "Alternate Mobile Number",
			"id" :"alternate_mobile_number",
			"type": "text",		
			"maxlength" : 10
		},
		{
			"label": "Landline Number",
			"id" :"landline_number",
			"type": "text",
			"required": false
		},
		{
			"label": "Email ID",
			"id" :"email_id",
			"type": "email",
			"required": true
		}		
		]
}`;